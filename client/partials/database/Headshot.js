import './Headshot.html';

var rotation = {
  1: 'rotate(0deg)',
  3: 'rotate(180deg)',
  6: 'rotate(90deg)',
  8: 'rotate(270deg)'
};

Template.Headshot.onCreated = function(){
 Session.set('headshotUploadDone', 'no');
 Session.set('imageName', '');
 Session.set('imageNameSize', '');
 Session.set('dataUrl', '');
};

Template.Headshot.onRendered = function() {
  Session.set('dataUrl', '');
  Session.set('headshotUploadDone', 'no');
  Session.set('imageName', '');
  Session.set('imageNameSize', '');
};

Template.Headshot.helpers({
  imageName: function() {
    return Session.get('imageNameSize');
  },
  headshotUploadDone: function(component) {
    if(Session.get('headshotUploadDone') == 'yes' || Session.get('headshotUploadDone') == true) {
      switch (component) {
        case 'i':
          return 'fa-check';
          break;
        case 'btn':
          return 'btn-success';
          break;
      }
    } else if(Session.get('headshotUploadDone') == 'no' || Session.get('headshotUploadDone') == false) {
      switch (component) {
        case 'i':
          return 'fa-cloud-upload';
          break;
        case 'btn':
          return 'btn-info';
          break;
      }
    } else if(Session.get('headshotUploadDone') == 'ongoing') {
      switch (component) {
        case 'i':
          return 'fa-spinner fa-pulse';
          break;
        case 'btn':
          return 'btn-warning';
          break;
      }
    } else {
      switch (component) {
        case 'i':
          return 'fa-cloud-upload';
          break;
        case 'btn':
          return 'btn-info';
          break;
      }
    }
  }
});

Template.Headshot.events({
  'hidden.bs.modal #modalHeadshot': function(event) {
    Session.set('dataUrl', '');
    Session.set('headshotUploadDone', 'no');
    Session.set('imageName', '');
    Session.set('imageNameSize', '');
    console.log(event.target.hidden);
  },
  'click #btnHeadshotClose': function(event) {
    // Close modal and reset all session variables
    event.preventDefault();
    Session.set('dataUrl', '');
    Session.set('headshotUploadDone', 'no');
    Session.set('imageName', '');
    Session.set('imageNameSize', '');
    Modal.hide('Headshot');
  },
  'change :file': function(event) {
    // After clicking 'Select' button, grab event target and analyze file
    var files = event.target.files;
    if (files.length === 0) {
      return;
    }
    var file = files[0];
    // Size limit of 2mb
    if (file.size > 2097152) {
      console.log('too big! ' + file.size);
      //Session.set('imageName', '2 MB max');
      Session.set('imageNameSize', 'Image too large! 2 MB max');
      toastr.error('Image too large!<br>2 MB max.');
      return;
    }
    // Grab filename and size and send back for textbox
    tempImageSize = formatBytes(file.size);
    tempImageName = file.name + ' (' + tempImageSize + ')';
    Session.set('imageName', file.name);
    Session.set('imageNameSize', tempImageName);
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
      var dataUrl = event.target.result;
      Session.set('dataUrl', dataUrl);
    }
    fileReader.readAsDataURL(file);
  },
  'click #btnClearPicture': function (event) {
    event.preventDefault();
    Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
    Session.set('dataUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
    Modal.hide('Headshot');
    return;
  },
  'submit': function(event, template) {
    event.preventDefault();
    if (Session.get('dataUrl') == '') {
      toastr.error('No file selected');
    } else {
      Session.set('headshotUploadDone', 'ongoing');
      Session.set('imageUrl', Session.get('dataUrl'));
      Session.set('headshotUploadDone', 'yes');
      Session.set('imageName', '');
      Session.set('imageNameSize', '');
      Session.set('headshotUploadDone', 'no');
      Session.set('headshotChanged', true);
      Modal.hide('Headshot');
      return;
    }
  },
});

// Make size all purrty
function formatBytes(bytes, decimals) {
  if(bytes == 0) return '0 Byte';
   var k = 1000; // or 1024 for binary
   var dm = decimals + 1 || 3;
   var sizes = ['B', 'KB', 'MB'];
   var i = Math.floor(Math.log(bytes) / Math.log(k));
   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
/*
function _arrayBufferToBase64( buffer ) {
  var binary = ''
  var bytes = new Uint8Array( buffer )
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode( bytes[ i ] )
  }
  return window.btoa( binary );
}

/*var orientation = function (base64, callback) {

}*/