import './PermsUser.html';

Template.PermsUser.helpers({
  user: function() {
    var userSession = Session.get('usercontrol');
    return Meteor.users.findOne({_id: userSession});
  },
  databaseAccess: function() {
    if ($('.chkDatabaseAccess').prop('checked')) {
      Session.set('DatabaseAccess', '');
    } else {
      Session.set('DatabaseAccess', 'disabled');
    }
    return Session.get('DatabaseAccess');
  }
});

Template.PermsUser.events({
  // Do a bit of jQuery magic to get the 'All' button to work
  'click #chkBoxOfficeAll': function(event) {
    if (event.target.checked) {
      $('.chkBoxOffice').prop('checked', true);
    } else {
      $('.chkBoxOffice').prop('checked', false);
    }
  },
  'click .chkBoxOffice': function(event) {
    if (!event.target.checked) {
      $('.chkBoxOfficeAll').prop('checked', false);
    }
  },
  'click #chkShowsAll': function(event) {
    if (event.target.checked) {
      $('.chkShows').prop('checked', true);
    } else {
      $('.chkShows').prop('checked', false);
    }
  },
  'click .chkShows': function(event) {
    if (!event.target.checked) {
      $('.chkShowsAll').prop('checked', false);
    }
  },
  'click #chkCustomersAll': function(event) {
    if (event.target.checked) {
      $('.chkCustomers').prop('checked', true);
    } else {
      $('.chkCustomers').prop('checked', false);
    }
  },
  'click .chkCustomers': function(event) {
    if (!event.target.checked) {
      $('.chkBoxOfficeAll').prop('checked', false);
    }
  },
  'click #chkVolunteersAll': function(event) {
    if (event.target.checked) {
      $('.chkVolunteers').prop('checked', true);
    } else {
      $('.chkVolunteers').prop('checked', false);
    }
  },
  'click .chkVolunteers': function(event) {
    if (!event.target.checked) {
      $('.chkVolunteersAll').prop('checked', false);
    }
  },
  'click #chkMiscAll': function(event) {
    if (event.target.checked) {
      $('.chkMisc').prop('checked', true);
    } else {
      $('.chkMisc').prop('checked', false);
    }
  },
  'click .chkMisc': function(event) {
    if (!event.target.checked) {
      $('.chkMiscAll').prop('checked', false);
    }
  },
  'click .chkDatabaseAccess': function(event) {
    // Disable or Enable access to the Database portion of perms as needed
    if (event.target.checked) {
      Session.set('DatabaseAccess', '');
      $('.chkCustomers').prop('disabled', false)
      $('.chkCustomersAll').prop('disabled', false)
      $('.chkVolunteers').prop('disabled', false)
      $('.chkVolunteersAll').prop('disabled', false)
      $('.chkMisc').prop('disabled', false)
      $('.chkMiscAll').prop('disabled', false)
      $('.chkSearch').prop('disabled', false)
    } else {
      Session.set('DatabaseAccess', 'disabled');
      $('.chkCustomers').prop('disabled', true)
      $('.chkCustomersAll').prop('disabled', true)
      $('.chkVolunteers').prop('disabled', true)
      $('.chkVolunteersAll').prop('disabled', true)
      $('.chkMisc').prop('disabled', true)
      $('.chkMiscAll').prop('disabled', true)
      $('.chkSearch').prop('disabled', true)
      $('.chkCustomers').prop('checked', false)
      $('.chkCustomersAll').prop('checked', false)
      $('.chkVolunteers').prop('checked', false)
      $('.chkVolunteersAll').prop('checked', false)
      $('.chkMisc').prop('checked', false)
      $('.chkMiscAll').prop('checked', false)
      $('.chkSearch').prop('checked', false)
    }
  },
  'submit .permsuser': function(event) {
    // Submit permissions to server for saving in database
    event.preventDefault();
    const target = event.target;
    var userSession = Session.get('usercontrol');
    var setPermsData = {
      boxoffice: {
        access: target.chkBoxOfficeAccess.checked,
        create: target.chkBoxOfficeCreate.checked,
        edit: target.chkBoxOfficeEdit.checked,
        delete: target.chkBoxOfficeDelete.checked
      },
      shows: {
        access: target.chkShowsAccess.checked,
        create: target.chkShowsCreate.checked,
        edit: target.chkShowsEdit.checked,
        delete: target.chkShowsDelete.checked
      },
      database: {
        access: target.chkDatabaseAccess.checked,
        search: target.chkSearch.checked,
        customers: {
          access: target.chkCustomersAccess.checked,
          create: target.chkCustomersCreate.checked,
          edit: target.chkCustomersEdit.checked,
          delete: target.chkCustomersDelete.checked
        },
        volunteers: {
          access: target.chkVolunteersAccess.checked,
          create: target.chkVolunteersCreate.checked,
          edit: target.chkVolunteersEdit.checked,
          delete: target.chkVolunteersDelete.checked
        },
        misc: {
          access: target.chkMiscAccess.checked,
          create: target.chkMiscCreate.checked,
          edit: target.chkMiscEdit.checked,
          delete: target.chkMiscDelete.checked,
        }
      },
      admin: target.chkAdmin.checked
    };

    //console.log(event);
    Meteor.call('adminSetPerms', userSession, setPermsData);
    toastr['success']('Permissions saved', "Success");
    Session.set('usercontrol', '');
    Modal.hide('PermsUser');
  }
});
