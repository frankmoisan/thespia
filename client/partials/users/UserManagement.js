import './UserManagement.html';
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.UserManagement.events({
  'click .btn-edit': function(event) {
    // Bouton 'Editer' - Show Modal
    event.preventDefault();
    Session.set('usercontrol', event.toElement.id);
    Modal.show('EditUser');
  },
  'click .btn-password': function(event) {
    // Bouton 'Mot de passe'
    event.preventDefault();
    Session.set('usercontrol', event.toElement.id);
    Modal.show('ResetPassword');
  },
  'click .btn-delete': function(event) {
    // Bouton 'Supprimer'
    event.preventDefault();
    Session.set('usercontrol', event.toElement.id);
    Modal.show('DeleteUser');
  },
  'click .adduser': function(event) {
    // Show the 'Add User' modal
    event.preventDefault();
    Modal.show('AddUser');
  },
  'click .btn-rights': function(event) {
    // Access perms modal
    event.preventDefault();
    Session.set('usercontrol', event.toElement.id);
    Modal.show('PermsUser');
  }
})

Template.UserManagement.helpers({
  allusers: function() {
    if (Session.get('dbLoaded')) {
      return Meteor.users.find({ $where: "this.username != 'admin'"}).fetch()
    }
  },
  phonenumber: function(number) {
    let preParsed;
    let postParsed;
    try {
      preParsed = phoneUtil.parse(number, 'CA');
      postParsed = phoneUtil.format(preParsed, PNF.NATIONAL);
    }
    catch(err) {
      postParsed = number;
    }
    return postParsed
  },
  currentUser: function(id) {
    return id === Meteor.userId();
  }
});
