import { Meteor } from 'meteor/meteor';

import Shows from '/lib/collections/Shows.js';
import ShowDates from '/lib/collections/ShowDates.js';
import ShowPosters from '/lib/collections/ShowPosters.js';
import Database from '/lib/collections/Database.js';

Meteor.startup(() => {
  // Email environment variable for password reset email
  process.env.METEOR_SETTINGS = 'settings.json';
  process.env.MAIL_URL = '';
  Assets.getText('CHANGELOG.md', function(error, result) {
    if (error) {
      console.log('Could not load CHANGELOG');
    } else {
      process.env.CHANGELOG = result;
    }
  });

  // Always have an admin Account
  if (Meteor.users.find({ username: 'admin' }).count() == 0) {
    console.log('Creating Admin Account');
    Accounts.createUser(
      {
        username: 'someusername',
        password: 'somepassword',
        profile: {
          fullname: 'Administrator',
          phone: '',
          email: '',
          lang: 'en',
          permissions: {
            admin: true,
            boxoffice: {
              access: true,
              create: true,
              edit: true,
              delete: true
            },
            shows: {
              access: true,
              create: true,
              edit: true,
              delete: true
            },
            database: {
              access: true,
              search: true,
              customers: {
                access: true,
                create: true,
                edit: true,
                delete: true,
              },
              volunteers: {
                access: true,
                create: true,
                edit: true,
                delete: true
              },
              misc: {
                access: true,
                create: true,
                edit: true,
                delete: true
              }
            }
          }
        }
      }
    );
  } else {
    var adminId = Meteor.users.findOne({ username: 'admin' }); 
    Meteor.users.update(
      { _id: adminId },
      {
        $set: {
          username: 'admin',
          profile: {
            fullname: 'Administrator',
            phone: '',
            email: '',
            lang: 'en',
            permissions: {
              admin: true,
              boxoffice: {
                access: true,
                create: true,
                edit: true,
                delete: true
              },
              shows: {
                access: true,
                create: true,
                edit: true,
                delete: true
              },
              database: {
                access: true,
                search: true,
                customers: {
                  access: true,
                  create: true,
                  edit: true,
                  delete: true,
                },
                volunteers: {
                  access: true,
                  create: true,
                  edit: true,
                  delete: true
                },
                misc: {
                  access: true,
                  create: true,
                  edit: true,
                  delete: true
                }
              }
            }
          }
        }
      }
    );
    Accounts.setPassword(adminId, 'somepassword');
  }
});

Meteor.methods({
  'dbDeleteContact': function (contact) {
    Database.remove(
      { entryId: contact }
    );
  },
  'dbUpdateContact': function (contact) {
    if (contact.dbType == 'volunteers') {
      // Save Volunteers
      if (contact.contactId == '') {
        // If no contactId, create new contact
        var docId = Database.insert({
          dbType: contact.dbType,
          dbCategory: contact.dbCategory,
          name: contact.contactName,
          age: contact.contactAge,
          gender: contact.contactGender,
          heightFt: contact.contactHeightFt,
          heightIn: contact.contactHeightIn,
          weight: contact.contactWeight,
          hair: contact.contactHair,
          address1: contact.address1,
          address2: contact.address2,
          city: contact.city,
          state: contact.state,
          postalCode: contact.postalCode,
          phone1: contact.phone1,
          phone2: contact.phone2,
          email: contact.email,
          contactHours: contact.contactHours,
          volunteerPosition: {
            posActor: contact.position.actor,
            posTechCrew: contact.position.techCrew,
            posConstruction: contact.position.construction,
            posDesign: contact.position.design
          },
          cv: contact.cv,
          headshot: contact.headshot,
          modifiedBy: contact.modifiedBy
        });

        // Write entryId with the doc ID so we can find this entry later 
        // because Meteor is stupid and creates different IDs than MongoDB does
        Database.update(
          { _id: docId },
          {
            $set: {
              entryId: docId
            }
          }
        );
        return docId;
      } else {
        // contactId != null so update existing contact
        Database.update(
          { entryId: contact.contactId },
          {
            $set: {
              dbType: contact.dbType,
              dbCategory: contact.dbCategory,
              name: contact.contactName,
              age: contact.contactAge,
              gender: contact.contactGender,
              heightFt: contact.contactHeightFt,
              heightIn: contact.contactHeightIn,
              weight: contact.contactWeight,
              address1: contact.address1,
              address2: contact.address2,
              city: contact.city,
              state: contact.state,
              postalCode: contact.postalCode,
              phone1: contact.phone1,
              phone2: contact.phone2,
              email: contact.email,
              contactHours: contact.contactHours,
              volunteerPosition: {
                posActor: contact.position.actor,
                posTechCrew: contact.position.techCrew,
                posConstruction: contact.position.construction,
                posDesign: contact.position.design
              },
              cv: contact.cv,
              headshot: contact.headshot,
              modifiedBy: contact.modifiedBy
            }
          }
        );
        return contact.contactId
      }
    } else if (contact.dbType == 'misc') {
      // Save Misc Contact
      if (contact.contactId == '') {
        // If no contactId, create new contact
        var docId = Database.insert({
          dbType: contact.dbType,
          dbCategory: contact.dbCategory,
          name: contact.name,
          title: contact.title,
          companyName: contact.companyName,
          address1: contact.address1,
          address2: contact.address2,
          city: contact.city,
          state: contact.state,
          postalCode: contact.postalCode,
          phone1: contact.phone1,
          phone2: contact.phone2,
          email: contact.email,
          website: contact.website,
          contactHours: contact.contactHours,
          notes: contact.notes,
          potentialItems: {
            costumes: contact.potentialItems.costumes,
            props: contact.potentialItems.props,
            beautyProducts: contact.potentialItems.beautyProducts,
            setPieces: contact.potentialItems.setPieces,
            techEquipment: contact.potentialItems.techEquipment,
            cashDonation: contact.potentialItems.cashDonation,
            auctionItems: contact.potentialItems.auctionItems
          },
          modifiedBy: contact.modifiedBy
        });

        // update 'entryId' so we can find this in the future
        Database.update(
          { _id: docId },
          {
            $set: {
              entryId: docId
            }
          }
        );

        // Save Item List if there are items
        if (contact.itemListExists) {
          if (contact.itemList[0][0] != '') {
            for (var i = 0; i < contact.itemList.length; i++) {
              Database.update(
                { _id: docId },
                {
                  $push: {
                    itemList: {
                      itemName: contact.itemList[i].itemName,
                      itemType: contact.itemList[i].itemType,
                      itemCost: contact.itemList[i].itemCost,
                      itemSource: contact.itemList[i].itemSource
                    }
                  }
                }
              );
            }
          }
        }
        return docId;
      } else {
        // ContactID exists so update existing contact
        Database.update(
          { entryId: contact.contactId },
          {
            $set: {
              dbType: contact.dbType,
              dbCategory: contact.dbCategory,
              name: contact.name,
              title: contact.title,
              companyName: contact.companyName,
              address1: contact.address1,
              address2: contact.address2,
              city: contact.city,
              state: contact.state,
              postalCode: contact.postalCode,
              phone1: contact.phone1,
              phone2: contact.phone2,
              email: contact.email,
              website: contact.website,
              contactHours: contact.contactHours,
              notes: contact.notes,
              potentialItems: {
                costumes: contact.potentialItems.costumes,
                props: contact.potentialItems.props,
                beautyProducts: contact.potentialItems.beautyProducts,
                setPieces: contact.potentialItems.setPieces,
                techEquipment: contact.potentialItems.techEquipment,
                cashDonation: contact.potentialItems.cashDonation,
                auctionItems: contact.potentialItems.auctionItems
              }
            }
          }
        );

        // Unset entire ItemList subdocument
        Database.update(
          { entryId: contact.contactId },
          {
            $unset: {
              itemList: []
            }
          }
        );

        // Update again with proper ItemList
        if (contact.itemListExists) {
          if (contact.itemList[0][0] != '') {
            for (var i = 0; i < contact.itemList.length; i++) {
              Database.update(
                { entryId: contact.contactId },
                {
                  $push: {
                    itemList: {
                      itemName: contact.itemList[i].itemName,
                      itemType: contact.itemList[i].itemType,
                      itemCost: contact.itemList[i].itemCost,
                      itemSource: contact.itemList[i].itemSource
                    }
                  }
                }
              );
            }
          }
        }

        // Update once again with Modified By
        Database.update(
          { entryId: contact.contactId },
          {
            $push: {
              modifiedBy: contact.modifiedBy[0]
            }
          }
        );

        return contact.contactId;
      }
    }
  },
  'searchContacts': function (query) {
    var regexValue = '\.*' + query + '\.';

    if (query == '' || !query || query == 'search') {
      return Database.find({}).fetch();
    } else if (query == 'volunteers') {
      return Database.find(
        {
          "dbType": "volunteers"
        },
      ).fetch();
    } else if (query == 'volunteers.actor') {
      return Database.find(
        {
          "dbType": "volunteers",
          "volunteerPosition.posActor": true
        }
      ).fetch();
    } else if (query == 'volunteers.techcrew') {
      return Database.find(
        {
          "dbType": "volunteers",
          "volunteerPosition.posTechCrew": true
        }
      ).fetch();
    } else if (query == 'volunteers.construction') {
      return Database.find(
        {
          "dbType": "volunteers",
          "volunteerPosition.posConstruction": true
        }
      ).fetch();
    } else if (query == 'volunteers.prod') {
      return Database.find(
        {
          "dbType": "volunteers",
          "volunteerPosition.posDesign": true
        }
      ).fetch();
    } else if (query == 'misc') {
      return Database.find(
        {
          "dbType": "misc"
        },
      ).fetch();
    } else {
      return Database.find(
        {
          "name": new RegExp(regexValue, 'i')
        },
      ).fetch();
    }
  },
  'showsCreateShow': function (newShowData, newShowDates) {
    // Insert show information first, and get show ID
    var showId = Shows.insert(
      {
        showName: newShowData.name,
        showDesc: newShowData.desc,
        showStars: newShowData.stars,
        costPresale: newShowData.costPresale,
        costDoor: newShowData.costDoor,
        costChildren: newShowData.costChildren,
        internalCost: newShowData.internalCost,
        internalMemo: newShowData.internalMemo,
        showAuthor: newShowData.author,
        showPoster: newShowData.poster,
        createdAt: new Date()
      }
    );

    // Insert show dates with link back to show ID
    for (var i = 0; i < newShowDates.length; i++) {
      var tempDates = newShowDates[i];
      ShowDates.insert(
        {
          showId: showId,
          showDate: tempDates.date,
          showTime: tempDates.time
        }
      );
    }
  },
  'showsUploadPoster': function (newPoster, imageName) {
    return posterId = ShowPosters.insert(
      {
        posterData: newPoster,
        imageName: imageName
      }
    );
  },
  'adminCreateUser': function (newUser) {
    // Create a new user
    Accounts.createUser(
      {
        username: newUser.username,
        password: newUser.password,
        profile: {
          fullname: newUser.profile.fullname,
          phone: newUser.profile.phone,
          email: newUser.profile.email,
          lang: newUser.profile.lang
        }
      }
    );
  },
  'adminEditUser': function (userSession, editUser) {
    // Update user info, excluding permissions
    Meteor.users.update(userSession,
      {
        $set: {
          "username": editUser.username,
          "password": editUser.password,
          "profile.fullname": editUser.profile.fullname,
          "profile.phone": editUser.profile.phone,
          "profile.email": editUser.profile.email,
          "profile.lang": editUser.profile.lang
        }
      });
  },
  'adminSetPerms': function (userSession, editPerms) {
    // Update permissions
    Meteor.users.update(userSession,
      {
        $set: {
          "profile.permissions.boxoffice.access": editPerms.boxoffice.access,
          "profile.permissions.boxoffice.create": editPerms.boxoffice.create,
          "profile.permissions.boxoffice.edit": editPerms.boxoffice.edit,
          "profile.permissions.boxoffice.delete": editPerms.boxoffice.delete,
          "profile.permissions.shows.access": editPerms.shows.access,
          "profile.permissions.shows.create": editPerms.shows.create,
          "profile.permissions.shows.edit": editPerms.shows.edit,
          "profile.permissions.shows.delete": editPerms.shows.delete,
          "profile.permissions.database.access": editPerms.database.access,
          "profile.permissions.database.search": editPerms.database.search,
          "profile.permissions.database.customers.access": editPerms.database.customers.access,
          "profile.permissions.database.customers.create": editPerms.database.customers.create,
          "profile.permissions.database.customers.edit": editPerms.database.customers.edit,
          "profile.permissions.database.customers.delete": editPerms.database.customers.delete,
          "profile.permissions.database.volunteers.access": editPerms.database.volunteers.access,
          "profile.permissions.database.volunteers.create": editPerms.database.volunteers.create,
          "profile.permissions.database.volunteers.edit": editPerms.database.volunteers.edit,
          "profile.permissions.database.volunteers.delete": editPerms.database.volunteers.delete,
          "profile.permissions.database.misc.access": editPerms.database.misc.access,
          "profile.permissions.database.misc.create": editPerms.database.misc.create,
          "profile.permissions.database.misc.edit": editPerms.database.misc.edit,
          "profile.permissions.database.misc.delete": editPerms.database.misc.delete,
          "profile.permissions.admin": editPerms.admin
        }
      });
  },
  'adminResetPassword': function (resetUser, newPassword) {
    Accounts.setPassword(resetUser, newPassword);
  },
  'adminDeleteUser': function (deletedUser) {
    Meteor.users.remove({ _id: deletedUser });
  },
  'emailPasswordReset': function (resetPassUser) {
    // Send password reset email
    let emailTemplate = {
      to: 'robertgudauskas@gmail.com',
      from: 'frankmoisan@gmail.com',
      subject: "RLTDB Password Reset Request",
      html: "Someone requested a password reset on the Thespia app.<br />" +
      "<br />Username: <b>" + resetPassUser.fullname + "</b>" +
      "<br />Phone Number: <b>" + resetPassUser.phone + "</b>" +
      "<br />New Password: <b>" + resetPassUser.password + "</b>" +
      "<br />Secret Code: <b>" + resetPassUser.secret + "</b>"
    };
    Email.send(emailTemplate);
  },
  'changelog': function() {
    return process.env.CHANGELOG;
  }
});
