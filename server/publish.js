
Meteor.publish('AllUsers', function () {
  return Meteor.users.find({}, { fields: { username: 1, fullname: 1, profile: 1 } });
});

Meteor.publish('CurrentUser', function () {
  return Meteor.users.find({ _id: this.userId }, { fields: { username: 1, profile: 1 } });
});

Meteor.publish('AllShows', function () {
  return Shows.find({});
});

Meteor.publish('AllShowDates', function () {
  return ShowDates.find({});
});

Meteor.publish('AllShowPosters', function () {
  return ShowPosters.find({});
});

Meteor.publish('AllDatabase', function () {
  return Database.find({});
});

Meteor.publish('DatabaseItems', function () {
  return DatabaseItems.find({});
});