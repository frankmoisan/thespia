import './MiscContactsSingle.html';
var handle = Meteor.subscribe('AllDatabase');

Template.MiscContactsSingle.onCreated(function () {
  this.currentEntry = new ReactiveVar('');
  Session.set('miscItemList', 1);
  Session.set('resetItemList', false);
  if (FlowRouter.getParam('_id')) {
    Session.set('miscSpinner', true);
  } else {
    Session.set('miscSpinner', false);
  }

});

Template.MiscContactsSingle.onRendered(function () {
  if (FlowRouter.getParam('_id')) {
    this.entryId = new ReactiveVar(FlowRouter.getParam('_id'));
    Session.set('miscSpinner', true);
    this.autorun(function (thisComp) {
      if (handle.ready()) {
        var currentEntry = Database.findOne({ entryId: Template.instance().entryId.get() });
        Template.instance().currentEntry.set(currentEntry);
        Session.set('miscSpinner', false);
        Session.set('miscItemList', currentEntry.itemList);
        thisComp.stop();
      }

    });
  } else {
    this.entryId = new ReactiveVar(false);
  }
});

Template.MiscContactsSingle.helpers({
  getItemListNumber: function () {
    return Session.get('miscItemList');
  },
  itemListMinusDisabled: function () {
    if (Session.get('miscItemList') == 1) {
      return 'disabled';
    } else {
      return;
    }
  },
  loopCount: function (count) {
    var countArr = [];
    for (var i = 0; i < count; i++) {
      countArr.push(i);
    }
    return countArr;
  },
  currentEntry: function () {
    if (FlowRouter.getParam('_id') && (handle.ready())) {
      return Template.instance().currentEntry.get();
    }
    return;
  },
  showSpinner: function () {
    return Session.get('miscSpinner');
  },
  isDisabled: function (button) {
    var thisUser = Meteor.user().profile.permissions.database;
    switch (button) {
      case 'save':
        if (FlowRouter.getParam('_id')) {
          if (!thisUser.misc.edit) {
            return 'disabled';
          }
        } else if (!thisUser.misc.create) {
          return 'disabled';
        }
        break;
      case 'delete':
        if (!thisUser.misc.delete || !FlowRouter.getParam('_id')) {
          return 'disabled';
        }
        break;
      default:
        return;
    }
    return;
  },
  potentialItems: function (option) {
    if (FlowRouter.getParam('_id') && Template.instance().currentEntry.get() && (handle.ready())) {
      var entryOpt = Template.instance().currentEntry.get();
      switch (option) {
        case 'costumes':
          if (entryOpt.potentialItems.costumes) {
            return 'checked';
          }
          break;
        case 'props':
          if (entryOpt.potentialItems.props) {
            return 'checked';
          }
          break;
        case 'beauty':
          if (entryOpt.potentialItems.beautyProducts) {
            return 'checked';
          }
          break;
        case 'setpieces':
          if (entryOpt.potentialItems.setPieces) {
            return 'checked';
          }
          break;
        case 'tech':
          if (entryOpt.potentialItems.techEquipment) {
            return 'checked';
          }
          break;
        case 'cash':
          if (entryOpt.potentialItems.cashDonation) {
            return 'checked';
          }
          break;
        case 'auctions':
          if (entryOpt.potentialItems.auctionItems) {
            return 'checked';
          }
          break;
        default:
          return;
      }
      return;
    }
    return;
  }
});

Template.MiscContactsSingle.events({
  'click .itemListPlus': function (event) {
    // Add items
    event.preventDefault();
    let currentNumberOfItems = Session.get('miscItemList');
    currentNumberOfItems++;
    Session.set('miscItemList', currentNumberOfItems);
  },
  'click .itemListMinus': function (event) {
    // Remove items
    event.preventDefault();
    let currentNumberOfItems = Session.get('miscItemList');
    currentNumberOfItems--;
    Session.set('miscItemList', currentNumberOfItems);
  },
  'reset .misccontactform': function (event) {
    Session.set('miscItemList', 1);
    $('.itemCostInput').prop('disabled', false);
    $('#listItem0').text('');
    $('#btnItemType0').val('Type')
    $('#btnItemSource0').val('Source');
  },
  'click #btnReturnMisc': function (event) {
    Session.set('miscItemList', 1);
    $('.itemCostInput').prop('disabled', false);
    FlowRouter.go('misc');
  },
  'click .btnContactDelete': function (event) {
    event.preventDefault();
    if (FlowRouter.getParam('_id')) {
      var thisBtn = $('.btnContactDelete');
      if (thisBtn.hasClass('btnPost')) {
        Meteor.call('dbDeleteContact', FlowRouter.getParam('_id'), function (error, success) {
          if (error) {
            toastr.error('Could not delete contact!', 'Error');
            console.log(error);
          } else {
            toastr.success('Contact deleted.', 'Success');
            FlowRouter.go('misc');
          }
        });
      }
      if (thisBtn.hasClass('btnPre')) {
        thisBtn.removeClass('btnPre').addClass('btnPost').text('CONFIRM?');
        setTimeout(function () {
          thisBtn.removeClass('btnPost').addClass('btnPre').text('Delete');
        }, 5000);
      }
    }
  },
  'click .btnControl': function (event) {
    event.preventDefault();
    if (event.target.value == 'saveNew' || event.target.value == 'save') {

      // If items exist in the list, go through them and add them to results
      // each item is added to an object, which is then added to an array 
      /// ...because Mongodb.
      if ($('[name="listItem0"]').val() != '' || $('[name="listItem0"]').val() != null) {
        var itemListExists = true;
        var itemList = [];
        var itemArray = {};
        for (var i = 0; i < Session.get('miscItemList'); i++) {
          var arrayItemCost;
          if (isNaN(parseFloat(($('[name="itemCost' + i + '"]').val())))) {
            arrayItemCost = 0;
          } else {
            arrayItemCost = parseFloat(($('[name="itemCost' + i + '"]').val()));
          }
          itemArray = {
            "itemName": $('[name="listItem' + i + '"]').val(),
            "itemType": $('[name="btnItemType' + i + '"]').text(),
            "itemCost": arrayItemCost,
            "itemSource": $('[name="btnItemSource' + i + '"]').text()
          };
          itemList.push(itemArray);
        }
      } else {
        var itemListExists = false;
      }

      var contactId;
      if (FlowRouter.getParam('_id')) {
        contactId = FlowRouter.getParam('_id');
      } else {
        contactId = '';
      }

      var results = {
        "contactId": contactId,
        "dbType": "misc",
        "dbCategory": '',
        "name": $('[name="contactName"]').val(),
        "title": $('[name="contactTitle"]').val(),
        "companyName": $('[name="contactCompany"]').val(),
        "address1": $('[name="contactAddress"]').val(),
        "address2": $('[name="contactAddress2"]').val(),
        "city": $('[name="contactCity"]').val(),
        "state": $('[name="contactState"]').val(),
        "postalCode": $('[name="contactPostalCode"]').val(),
        "phone1": $('[name="contactPhone1"]').val(),
        "phone2": $('[name="contactPhone2"]').val(),
        "email": $('[name="contactEmail"]').val(),
        "website": $('[name="contactWebsite"]').val(),
        "contactHours": $('[name="contactHours"]').val(),
        "notes": $('[name="contactMemo"]').val(),
        "itemListExists": itemListExists,
        "itemList": itemList,
        "potentialItems": {
          "costumes": $('[name="potentialCostumes"]').prop('checked'),
          "props": $('[name="potentialProps"]').prop('checked'),
          "beautyProducts": $('[name="potentialBeauty"]').prop('checked'),
          "setPieces": $('[name="potentialSetPieces"]').prop('checked'),
          "techEquipment": $('[name="potentialTech"]').prop('checked'),
          "cashDonation": $('[name="potentialCash"]').prop('checked'),
          "auctionItems": $('[name="potentialAuctions"]').prop('checked')
        },
        "modifiedBy": [{ "modifiedBy": Meteor.userId(), "modifiedOn": Date.now() }]
      };

      var resultContact = Meteor.call('dbUpdateContact', results, function (error, result) {
        if (error) {
          toastr.error(error.reason, 'Error');
          console.log(error);
        } else {
          toastr.success('Contact saved', 'Success');
          if (event.target.value == 'saveNew') {
            $('#btnContactReset').click();
            Session.set('miscItemList', 1);
            $('.itemCostInput').prop('disabled', false);
            $('#listItem0').val('');
            $('#btnItemType0').val('Type')
            $('#btnItemSource0').val('Source');
            Session.set('resetItemList', true);
            Session.set('miscSpinner', false);
            Template.instance().entryId.set('');
            FlowRouter.go('misc.new');
          }

          if (event.target.value == 'save') {
            FlowRouter.go('misc.single', { _id: result });
          }
        }
      });


    }
  }
});
