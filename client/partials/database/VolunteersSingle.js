import './VolunteersSingle.html';
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
var handle = Meteor.subscribe('AllDatabase');

Template.VolunteersSingle.onCreated(function () {
  this.currentEntry = new ReactiveVar();
  Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
  if (FlowRouter.getParam('_id')) {
    this.showSpinner = new ReactiveVar(true);
    Session.set('volunteerSpinner', true);
  } else {
    this.showSpinner = new ReactiveVar(false);
    Session.set('volunteerSpinner', false);
    $('.summernote').summernote('reset');
    Session.set('isActor', false);
  }
});

Template.VolunteersSingle.onRendered(function () {
  $('.summernote').summernote({
    dialogsInBody: true,
    height: 600,
    maxHeight: 600,
    toolbar: [
      ['style', ['style']],
      ['helpers', ['undo', 'redo']],
      ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
      ['font2', ['fontname']], ['font3', ['fontsize']], ['font4', ['color']],
      ['style', ['ul', 'ol', 'paragraph']], ['style2', ['height']]
    ]
  });

  $('#contactPhone1').on('focusout', function () {
    $('#contactPhone1').val(formatPhone($('#contactPhone1').val()));
  });
  $('#contactPhone2').on('focusout', function () {
    $('#contactPhone2').val(formatPhone($('#contactPhone2').val()));
  });

  Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
  Session.set('isActor', $('[name="contactPositionActor"]').prop('checked'));
  if (FlowRouter.getParam('_id')) {
    this.entryId = new ReactiveVar(FlowRouter.getParam('_id'));
    Template.instance().showSpinner.set(true);
    Session.set('volunteerSpinner', true);
    this.autorun(function (thisComp) {
      if (handle.ready()) {
        var currentEntry = Database.findOne({ entryId: Template.instance().entryId.get() });
        Template.instance().currentEntry.set(currentEntry);
        Template.instance().showSpinner.set(false);
        Session.set('volunteerSpinner', false);
        Session.set('imageUrl', Template.instance().currentEntry.get().headshot);
      }
      thisComp.stop();
    });
  } else {
    this.entryId = new ReactiveVar(false);
  }
});

Template.VolunteersSingle.helpers({
  'getHeadshot': function () {

    if (Session.get('imageUrl') || !Session.get('imageUrl', '') || Session.get('imageUrl') != 'undefined' || Session.get('imageUrl') != null) {
      return Session.get('imageUrl');
    } else {
      Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=")
      return Session.get('imageUrl');
    }

  },
  'getCV': function () {
    if (FlowRouter.getParam('_id') && handle.ready()) {
      currentCV = Template.instance().currentEntry.get();
      if (currentCV.cv || currentCV.cv != 'undefined' || currentCV.cv != '' || currentCV.cv != null) {
        $('#summernote').summernote('code', currentCV.cv);
        return;
        //return currentCV.cv;
      }
    }
  },
  'isActor': function () {
    return Session.get('isActor');
  },
  'headshotColumns': function () {
    if (Session.get('isActor')) {
      return 'col-md-6';
    } else {
      return 'col-md-12';
    }
  },
  'gender': function (getGender) {
    if (FlowRouter.getParam('_id') && handle.ready()) {
      var entryGender = Template.instance().currentEntry.get();
      if (getGender == entryGender.gender) {
        if (getGender == 'M') {
          $('#contactGenderLabelF').removeClass('active');
          $('#contactGenderLabelM').addClass('active');
        } else {
          $('#contactGenderLabelM').removeClass('active');
          $('#contactGenderLabelF').addClass('active');
        }
        return 'checked';
      } else {
        return;
      }
    }
    return;
  },
  'position': function (pos) {
    if (FlowRouter.getParam('_id') && handle.ready()) {
      var entryPos = Template.instance().currentEntry.get();
      switch (pos) {
        case 'actor':
          if (entryPos.volunteerPosition.posActor) {
            Session.set('isActor', true);
            return 'checked';
          }
          break;
        case 'techcrew':
          if (entryPos.volunteerPosition.posTechCrew) {
            return 'checked';
          }
          break;
        case 'construction':
          if (entryPos.volunteerPosition.posConstruction) {
            return 'checked';
          }
          break;
        case 'design':
          if (entryPos.volunteerPosition.posDesign) {
            return 'checked';
          }
          break;
        default:
          return;
      }
    }
    return;
  },
  isDisabled: function (button) {
    var thisUser = Meteor.user().profile.permissions.database;
    switch (button) {
      case 'save':
        if (FlowRouter.getParam('_id')) {
          if (!thisUser.volunteers.edit) {
            return 'disabled';
          }
        } else if (!thisUser.volunteers.create) {
          return 'disabled';
        }
        break;
      case 'delete':
        if (!thisUser.volunteers.delete || !FlowRouter.getParam('_id')) {
          return 'disabled';
        }
        break;
      default:
        return;
    }
    return;
  },
  'currentEntry': function () {
    if (FlowRouter.getParam('_id') && handle.ready()) {
      return Template.instance().currentEntry.get();
    }
    return;
  },
  'showSpinner': function () {
    return Session.get('volunteerSpinner');
  },
  'showForm': function () {
    return true;
  },
  phonenumber: function (number) {
    let preParsed;
    let postParsed;
    try {
      preParsed = phoneUtil.parse(number, 'CA');
      postParsed = phoneUtil.format(preParsed, PNF.NATIONAL);
    }
    catch (err) {
      postParsed = number;
    }
    return postParsed
  }
});

Template.VolunteersSingle.events({
  'click #btnCV': function (event) {
    event.preventDefault();
    if ($('#btnCVCaret').hasClass('fa-caret-right')) {
      $('#btnCVCaret').removeClass('fa-caret-right').addClass('fa-caret-down');
      $('html, body').animate({ scrollTop: $(document).height() }, 'fast');
    } else {
      $('#btnCVCaret').removeClass('fa-caret-down').addClass('fa-caret-right');
    }
  },
  'click #contactPositionActor': function (event) {
    Session.set('isActor', event.target.checked);
  },
  'click #imgHeadshot': function (event) {
    Session.set('dataUrl', '');
    //Session.set('imageUrl', '');
    Modal.show('Headshot');
  },
  'click .btnContactDelete': function (event) {
    event.preventDefault();
    if (FlowRouter.getParam('_id')) {
      var thisBtn = $('.btnContactDelete');
      if (thisBtn.hasClass('btnPost')) {
        Meteor.call('dbDeleteContact', FlowRouter.getParam('_id'), function (error, success) {
          if (error) {
            toastr.error('Could not delete contact!', 'Error');
            console.log(error);
          } else {
            toastr.success('Contact deleted.', 'Success');
            FlowRouter.go('volunteers');
          }
        });
      }
      if (thisBtn.hasClass('btnPre')) {
        thisBtn.removeClass('btnPre').addClass('btnPost').text('CONFIRM?');
        setTimeout(function() {
          thisBtn.removeClass('btnPost').addClass('btnPre').text('Delete');
        }, 5000);
      }
    }
  },
  'click .btnControl': function (event) {
    event.preventDefault();
    if (event.target.value == 'saveNew' || event.target.value == "save") {
      var contactId;
      if (FlowRouter.getParam('_id')) {
        contactId = FlowRouter.getParam('_id');
      } else {
        contactId = '';
      }
      var imageUrl = Session.get('imageUrl');
      if (imageUrl == '' || imageUrl == 'undefined' || imageUrl == null || !Session.get('imageUrl')) {
        imageUrl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=";
      }
      var results = {
        "contactId": contactId,
        "dbType": 'volunteers',
        'dbCategory': '',
        "contactName": $('[name="contactName"]').val(),
        "contactAge": $('[name="contactAge"]').val(),
        "contactGender": $('input[name=contactGender]:checked').val(),
        "contactHeightFt": $('[name="contactHeightFt"]').val(),
        "contactHeightIn": $('[name="contactHeightIn"]').val(),
        "contactWeight": $('[name="contactWeight"]').val(),
        "contactHair": $('[name="contactHair"]').val(),
        "address1": $('[name="contactAddress"]').val(),
        "address2": $('[name="contactAddress2"]').val(),
        "city": $('[name="contactCity"]').val(),
        "state": $('[name="contactState"]').val(),
        "postalCode": $('[name="contactPostalCode"]').val(),
        "phone1": $('[name="contactPhone1"]').val(),
        "phone2": $('[name="contactPhone2"]').val(),
        "email": $('[name="contactEmail"]').val(),
        "contactHours": $('[name="contactHours"]').val(),
        "position": {
          "actor": $('[name="contactPositionActor"]').prop('checked'),
          "techCrew": $('[name="contactPositionTechCrew"]').prop('checked'),
          "construction": $('[name="contactPositionConstruction"]').prop('checked'),
          "design": $('[name="contactPositionDesign"]').prop('checked')
        },
        "cv": $('.summernote').summernote('code'),
        "headshot": imageUrl,
        "modifiedBy": [{ "modifiedBy": Meteor.userId(), "modifiedOn": Date.now() }]
      };

      var entryId = Meteor.call('dbUpdateContact', results, function (error, result) {
        if (error) {
          toastr.error(error.reason, 'Error');
          console.log(error);
          return;
        } else {
          toastr.success('Volunteer saved.', 'Success');
          console.log(result);
          if (event.target.value == 'saveNew') {
            // Reset form and variables
            $('#btnContactReset').click();
            Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
            $('.summernote').summernote('reset');
            Session.set('isActor', false);
            FlowRouter.go('volunteers.new');
            return;
          }

          if (event.target.value == 'save') {
            //var path = 'volunteers/' + entryId;
            Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
            $('.summernote').summernote('reset');
            Session.set('isActor', false);
            FlowRouter.go('volunteers.single', { _id: result });
            return;
          }
        }
      });
    }
  },
  'reset .volunteersform': function (event) {
    Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
    $('.summernote').summernote('reset');
    Session.set('isActor', false);
  },
  'click #btnReturnVolunteers': function (event) {
    Session.set('imageUrl', "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=");
    $('.summernote').summernote('reset');
    Session.set('isActor', false);
    FlowRouter.go('volunteers');
  }
});

function formatPhone(number) {
  let preParsed;
  let postParsed;
  try {
    preParsed = phoneUtil.parse(number, 'CA');
    postParsed = phoneUtil.format(preParsed, PNF.NATIONAL);
  }
  catch (err) {
    console.log(err);
    postParsed = number;
  }
  return postParsed;
}