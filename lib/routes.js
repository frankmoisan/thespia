// Check for login and logout
if (Meteor.isClient) {
  Accounts.onLogin(function() {
    FlowRouter.go('home');
  });

  Accounts.onLogout(function() {
    FlowRouter.go('login');
  });
}

FlowRouter.triggers.enter([function(context, redirect) {
  if (!Meteor.userId()) {
    FlowRouter.go('login');
  }
}]);

FlowRouter.route('/', {
  name: 'home',
  action() {
    //BlazeLayout.render('MainLayout', {main: 'WelcomeLayout'});
    FlowRouter.go('shows');
  }
});

FlowRouter.route('/login', {
  name: 'login',
  action() {
    BlazeLayout.render('MainLayout', {main: 'LoginLayout'});
  }
});

FlowRouter.route('/logout', {
  name: 'logout',
  action() {
    Meteor.logout();
    FlowRouter.go('login');
  }
});

FlowRouter.route('/dashboard', {
  name: 'dashboard',
  action() {
    FlowRouter.go('home');
  }
});

FlowRouter.route('/boxoffice', {
  name: 'boxoffice'
});

FlowRouter.route('/changelog', {
  name: 'changelog',
  action() {
    BlazeLayout.render('ChangeLog');
  }
});


// SHOWS //
FlowRouter.route('/shows', {
  name: 'shows',
  action() {
    FlowRouter.go('shows.search');
  }
});

var showsRoute = FlowRouter.group({
  prefix: '/shows',
  name: 'shows.group'
});
showsRoute.route('/search', {
  name: 'shows.search',
  action() {
    BlazeLayout.render('MainLayout', {main: 'ShowsLayout'});
  }
});
showsRoute.route('/new', {
  name: 'shows.new',
  action() {
    BlazeLayout.render('MainLayout', {main: 'SingleShowLayout'});
  }
});
showsRoute.route('/:id', {
  name: 'shows.single',
  action() {
    BlazeLayout.render('MainLayout', {main: 'SingleShowsLayout'});
  }
});


// DATABASE //
FlowRouter.route('/database', {
  name: 'database',
  action() {
    FlowRouter.go('search')
  }
});

var databaseRoute = FlowRouter.group({
  prefix: '/database',
  name: 'database.group'
});
databaseRoute.route('/search', {
  name: 'search',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/misc', {
  name: 'misc',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/misc/new', {
  name: 'misc.new',
  action() {
    BlazeLayout.render('MainLayout', {main: 'MiscContactsLayout'})
  }
});
databaseRoute.route('/misc/:_id', {
  name: 'misc.single',
  action() {
    BlazeLayout.render('MainLayout', {main: 'MiscContactsLayout'})
  }
});
databaseRoute.route('/volunteers', {
  name: 'volunteers',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/volunteers/new', {
  name: 'volunteers.new',
  action() {
    BlazeLayout.render('MainLayout', {main: 'VolunteersLayout'})
  }
});
databaseRoute.route('/volunteers/actor', {
  name: 'volunteers.actor',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/volunteers/techcrew', {
  name: 'volunteers.techcrew',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/volunteers/construction', {
  name: 'volunteers.construction',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/volunteers/prod', {
  name: 'volunteers.prod',
  action() {
    BlazeLayout.render('MainLayout', {main: 'DatabaseLayout'})
  }
});
databaseRoute.route('/volunteers/:_id', {
  name: 'volunteers.single',
  action() {
    BlazeLayout.render('MainLayout', {main: 'VolunteersLayout'})
  }
});

FlowRouter.route('/admin', {
  name: 'admin',
  action() {
    BlazeLayout.render('MainLayout', {main: 'AdminLayout'});
  }
});
