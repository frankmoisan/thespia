import './EditUser.html';
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.EditUser.helpers({
  user: function() {
    var userSession = Session.get('usercontrol');
    return Meteor.users.findOne({_id: userSession});
  },
  selected: function(lang) {
    var userSession = Session.get('usercontrol');
    var currentUser = Meteor.users.findOne({_id: userSession});
    if (lang == currentUser.profile.lang) {
      return 'selected';
    }
  },
  phonenumber: function(number) {
    let preParsed;
    let postParsed;
    try {
      preParsed = phoneUtil.parse(number, 'CA');
      postParsed = phoneUtil.format(preParsed, PNF.NATIONAL);
    }
    catch(err) {
      postParsed = number;
    }
    return postParsed;
  }
});

Template.EditUser.events({
  'submit .edituser': function(event) {
    event.preventDefault();
    const target = event.target;
    var userSession = Session.get('usercontrol');
    var editUserData = {
      username: target.username.value,
      profile: {
        fullname: target.fullname.value,
        phone: target.phone.value,
        email: target.email.value,
        lang: target.lang.value
      }
    };

    Meteor.call('adminEditUser', userSession, editUserData);
    toastr['success']('User "' + editUserData.username + '" modified.', "Success");
    Session.set('usercontrol', '');
    Modal.hide('EditUser');
  },
  'click .close': function(event) {
    event.preventDefault();
    Session.set('usercontrol', '');
  }
});
