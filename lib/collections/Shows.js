var ShowsCollections = new Mongo.Collection("shows");

ShowsCollections.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

ModifiedBy = new SimpleSchema({
  modifiedBy: {
    type: String,
    optional: true
  },
  modifiedOn: {
    type: Date,
    optional: true
  }
});

ShowItems = new SimpleSchema({
  itemId: {
    type: String,
    optional: true
  }
});

ShowActors = new SimpleSchema({
  actorId: {
    type: String,
    optional: true
  }
});

ShowCollectionsSchema = new SimpleSchema({
  showTitle: {
    type: String,
    label: 'Nom du spectacle'
  },
  showDesc: {
    type: String,
    label: 'Description',
    optional: true
  },
  cost1: {
    type: String,
    label: 'Coût en prévente',
    optional: true
  },
  cost2: {
    type: String,
    label: "Coût à l'entrée",
    optional: true
  },
  cost3: {
    type: String,
    label: "Coût enfants",
    optional: true
  },
  showAuthor: {
    type: String,
    label: "Créateur",
    optional: true
  },
  showVenue: {
    type: String,
    optional: true
  },
  showPoster: {
    type: String,
    label: "Affiche du spectacle",
    optional: true
  },
  showFlyer: {
    type: String,
    optional: true
  },
  showInternalCost: {
    type: String,
    label: "Administrateur: Coût interne",
    optional: true
  },
  showInternalMemo: {
    type: String,
    label: "Administrateur: Memo interne",
    optional: true
  },
  showActors: {
    type: [ShowActors],
    optional: true
  },
  showItems: {
    type: [ShowItems],
    optional: true
  },
  modifiedBy: {
    type: [ModifiedBy],
    optional: true
  }
});

ShowsCollections.attachSchema(ShowCollectionsSchema);

export default Shows = ShowsCollections;
