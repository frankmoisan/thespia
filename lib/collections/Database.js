var DatabaseCollection = new Mongo.Collection("database");

DatabaseCollection.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

ModifiedBy = new SimpleSchema({
  modifiedBy: {
    type: String,
    optional: true
  },
  modifiedOn: {
    type: Date,
    optional: true
  }
});

PotentialItems = new SimpleSchema({
  costumes: {
    type: Boolean,
    optional: true
  },
  props: {
    type: Boolean,
    optional: true
  },
  beautyProducts: {
    type: Boolean,
    optional: true
  },
  setPieces: {
    type: Boolean,
    optional: true
  },
  techEquipment: {
    type: Boolean,
    optional: true
  },
  cashDonation: {
    type: Boolean,
    optional: true
  },
  auctionItems: {
    type: Boolean,
    optional: true
  }
});

ItemList = new SimpleSchema({
  itemName: {
    type: String,
    optional: true
  },
  itemType: {
    type: String,
    optional: true
  },
  itemCost: {
    type: Number,
    decimal: true,
    optional: true
  },
  itemSource: {
    type: String,
    optional: true
  }
});

VolunteerPosition = new SimpleSchema({
  posActor: {
    type: Boolean,
    optional: true
  },
  posTechCrew: {
    type: Boolean,
    optional: true
  },
  posConstruction: {
    type: Boolean,
    optional: true
  },
  posDesign: {
    type: Boolean,
    optional: true
  }
});

DatabaseCollectionSchema = new SimpleSchema({
  entryId: {
    type: String,
    optional: true
  },
  dbType: { // Customers, Volunteers, Misc
    type: String
  },
  dbCategory: { // General Admission, Friends and Family, etc
    type: String,
    optional: true
  },
  name: {
    type: String
  },
  title: {
    type: String,
    optional: true
  },
  companyName: {
    type: String,
    optional: true
  },
  address1: {
    type: String,
    optional: true
  },
  address2: {
    type: String,
    optional: true
  },
  city: {
    type: String,
    optional: true
  },
  state: {
    type: String,
    optional: true
  },
  postalCode: {
    type: String,
    optional: true
  },
  phone1: {
    type: String,
    optional: true
  },
  phone2: {
    type: String,
    optional: true
  },
  email: {
    type: String,
    optional: true
  },
  website: {
    type: String,
    optional: true
  },
  contactHours: {
    type: String,
    optional: true
  },
  age: {
    type: String,
    optional: true
  },
  gender: {
    type: String,
    optional: true
  },
  heightFt: {
    type: Number,
    optional: true
  },
  heightIn: {
    type: Number,
    optional: true
  },
  weight: {
    type: String,
    optional: true
  },
  hair: {
    type: String,
    optional: true
  },
  itemList: {
    type: Number,
    optional: true
  },
  volunteerPosition: {
    type: VolunteerPosition,
    optional: true
  },
  cv: {
    type: String,
    optional: true
  },
  headshot: {
    type: String,
    optional: true
  },
  notes: {
    type: String,
    optional: true
  },
  potentialItems: {
    type: PotentialItems,
    optional: true
  },
  modifiedBy: {
    type: [ModifiedBy],
    optional: true
  }
});

DatabaseCollection.attachSchema(DatabaseCollectionSchema);

export default Database = DatabaseCollection;
