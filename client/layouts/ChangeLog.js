import './ChangeLog.html'

Template.ChangeLog.onCreated(function () {
    Meteor.call('changelog', function (error, result) {
        if (error) {
            console.log(error);
        } else {
            console.log('Success!');
            Session.set('changelog', result);
        }
    });
});

Template.ChangeLog.helpers({
    changelog: function () {
        return Session.get('changelog');
    },
    test: function () {
        return 'Hello world!';
    }
});