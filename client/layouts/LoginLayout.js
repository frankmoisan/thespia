import './LoginLayout.html';

Template.LoginLayout.events({
  'submit #login-form': function(event, data) {
    event.preventDefault();
    var username = data.find('#login-username').value;
    var password = data.find('#login-password').value;

    // Validation
    var trimInput = function(val) {
      return val.replace(/^\s*|\s*$/g, "");
    }
    var username = trimInput(username);

    Meteor.loginWithPassword(username, password, function(err) {
      if (err) {
        Session.set('InvalidUser', 'true');
      } else {
        Session.set('InvalidUser', '');
        const thisUserPerms = Meteor.users.findOne({_id: Meteor.userId()});
        FlowRouter.go('home');
      }
    });
    return false;
  },
  'click .forgotpassword': function () {
    Modal.show('ForgotPassword');
  }
});

Template.LoginLayout.helpers({
  invaliduser: function() {
    if(Session.get('InvalidUser') == 'true') {
      return true;
    } else {
      return false;
    }
  },
  release: function() {
    return "Thespia 1.0.7"
  }
})
