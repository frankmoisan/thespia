Meteor.startup(function() {
  process.env.METEOR_SETTINGS = 'settings.json';

  Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY'
  });

  console.log('LANGUAGE: ' + TAPi18n.getLanguage());

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  Session.set('navDashboard', '');
  Session.set('navBoxOffice', '');
  Session.set('navShows', '');
  Session.set('navDatabase', 'active');
  Session.set('navAdmin', '');

});
