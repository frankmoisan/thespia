import './AllContacts.html';
Meteor.subscribe('AllDatabase');
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.AllContacts.onCreated(function () {
    Session.set('dbSearching', false)
    Session.set('dbFoundResults', false);
    $(document).on('keyup', function (keyEvent) {
        if (keyEvent.keyCode == 27) {
            $('#dbSearch').val('');
            Session.set('dbFoundResults', false);
            Session.set('dbSearching', false);
            FlowRouter.go('search');
        }
    });
});

Template.AllContacts.onRendered(function () {
    Tracker.autorun(function () {
        FlowRouter.watchPathChange();
        var currentRoute = FlowRouter.current().route.name;

        if (currentRoute != 'search') {
            Session.set('dbSearching', true)
            Session.set('dbFoundResults', false);

            Meteor.call('searchContacts', currentRoute, function (error, result) {
                if (error) {
                    toastr.error('Could not get results');
                    console.log(error);
                } else {
                    if (!result.length == 0) {
                        Session.set('searchResults', result);
                        Session.set('dbFoundResults', true);
                    }
                }
            });

            Session.set('dbSearching', false)
        } else {
            Session.set('dbSearching', false);
            Session.set('dbFoundResults', false);
        }

        switch (currentRoute) {
            case "volunteers":
                $('#dbSearch').val('Showing Volunteers');
                break;
            case "volunteers.actor":
                $('#dbSearch').val('Showing Volunteers with flag: Actor');
                break;
            case "volunteers.techcrew":
                $('#dbSearch').val('Showing Volunteers with flag: Tech Crew');
                break;
            case "volunteers.construction":
                $('#dbSearch').val('Showing Volunteers with flag: Construction');
                break;
            case "volunteers.prod":
                $('#dbSearch').val('Showing Volunteers with flag: Prod. Staff');
                break;
            case "misc":
                $('#dbSearch').val('Showing Misc Contacts');
                break;
            default:
                //$('#dbSearch').val('');
                break;
        }
    });
});

Template.AllContacts.onDestroyed(function () {
    $(document).off('keyup');
});

Template.AllContacts.helpers({
    foundResults: function () {
        return Session.get('dbFoundResults');
    },
    searching: function () {
        return Session.get('dbSearching');
    },
    results: function (tag) {
        if (tag == 'tags') {
            console.log('Tags!')
        } else {
            return Session.get('searchResults');
        }
    },
    isDisabled: function (dbType) {
        var thisUser = Meteor.user().profile.permissions.database;
        switch (dbType) {
            case 'volunteers':
                if (!thisUser.volunteers.delete) {
                    return 'disabled';
                }
                break;
            case 'misc':
                if (!thisUser.misc.delete) {
                    return 'disabled';
                }
                break;
            default:
                return;
        }
        return;
    },
    phonenumber: function (number) {
        let preParsed;
        let postParsed;
        try {
            preParsed = phoneUtil.parse(number, 'CA');
            postParsed = phoneUtil.format(preParsed, PNF.NATIONAL);
        }
        catch (err) {
            postParsed = number;
        }
        return postParsed
    }
});

Template.AllContacts.events({
    'submit .searchform': function (event) {
        event.preventDefault();
        search(event.target.dbSearch.value);
    },
    'click #btnReturnDatabase': function (event) {
        FlowRouter.go('search');
    },
    'click #btnNewVolunteer': function (event) {
        FlowRouter.go('volunteers.new');
    },
    'click #btnNewMisc': function (event) {
        FlowRouter.go('misc.new');
    },
    'click .btn-delete': function (event) {
        event.preventDefault();
        var thisBtn = $('#' + event.currentTarget.id);
        var thisSpan = $('#span' + event.currentTarget.id);
        if (thisBtn.hasClass('btnPost')) {
            Meteor.call('dbDeleteContact', event.currentTarget.id, function (error, success) {
                if (error) {
                    toastr.error('Could not delete contact!', 'Error');
                    console.log(error);
                } else {
                    toastr.success('Contact deleted.', 'Success');
                    search($('#dbSearch').val());
                }
            });
        }
        if (thisBtn.hasClass('btnPre')) {
            thisBtn.removeClass('btn-default btnPre').addClass('btn-danger btnPost');
            thisSpan.text('CONFIRM?');
            setTimeout(function () {
                thisBtn.removeClass('btn-danger btnPost');
                thisBtn.addClass('btn-default btnPre');
                thisSpan.text('Delete');
            }, 5000);
        }
    },
    'click .tag': function (event) {
        event.preventDefault();
    }
});

function search(event) {
    Session.set('dbFoundResults', false);
    Session.set('dbSearching', true);

    Meteor.call('searchContacts', event, function (error, result) {
        if (error) {
            toastr.error('Could not get results');
            console.log(error);
        } else {
            Session.set('searchResults', result);
        }
    });

    Session.set('dbFoundResults', true);
    Session.set('dbSearching', false);
}