import './ForgotPassword.html';

Template.ForgotPassword.events({
  'submit .forgotpassword': function(event) {
    event.preventDefault();
    const target = event.target;
    const resetPassInfo = {
      fullname: target.forgotPasswordFullName.value,
      phone: target.forgotPasswordPhone.value,
      secret: target.forgotPasswordSecret.value,
      password: target.forgotPasswordNewPassword.value
    };

    Meteor.call('emailPasswordReset', resetPassInfo);
    Modal.hide('ForgotPassword');
  }
})
