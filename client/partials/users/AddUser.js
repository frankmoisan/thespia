import './AddUser.html'

Template.AddUser.events({
  'submit .addnewuser': function (event) {
    event.preventDefault();
    const target = event.target;

    if (target.username.value == '' || target.password.value == '') {
      toastr.error("The Username and Passsword fields cannot be empty.", "Error");
    } else {
      var newUserData = {
        username: target.username.value,
        password: target.password.value,
        profile: {
          fullname: target.fullname.value,
          phone: target.phone.value,
          email: target.email.value,
          lang: target.lang.value
        }
      };

      if (Meteor.users.find({ "username": newUserData.username }).count()) {
        toastr.error('This user already exists!', 'Error');
      } else {
        Meteor.call('adminCreateUser', newUserData, function (error, success) {
          if (error) {
            console.log(error);
            toastr.error(error.result, 'Error');
          } else {
            toastr['success']('User "' + newUserData.username + '" created.', "Success");
          }
        });
        Modal.hide('AddUser');
      }
    }
  }
});
