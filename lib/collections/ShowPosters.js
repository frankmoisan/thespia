var ShowPostersCollections = new Mongo.Collection('showposters');

ShowPostersCollections.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

ShowPostersCollectionsSchema = new SimpleSchema({
  posterData: {
    type: String
  },
  imageName: {
    type: String
  }
});

ShowPostersCollections.attachSchema(ShowPostersCollectionsSchema);

export default ShowPosters = ShowPostersCollections;
