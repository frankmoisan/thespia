import './Poster.html';

Meteor.subscribe('AllShowPosters');

Template.Poster.onCreated = function(){
 Session.set('posterUploadDone', 'no');
};

Template.Poster.helpers({
  imageName: function() {
    return Session.get('imageNameSize');
  },
  posterUploadDone: function(component) {
    if(Session.get('posterUploadDone') == 'yes' || Session.get('posterUploadDone') == true) {
      switch (component) {
        case 'i':
          return 'fa-check';
          break;
        case 'btn':
          return 'btn-success';
          break;
      }
    } else if(Session.get('posterUploadDone') == 'no' || Session.get('posterUploadDone') == false) {
      switch (component) {
        case 'i':
          return 'fa-cloud-upload';
          break;
        case 'btn':
          return 'btn-info';
          break;
      }
    } else if(Session.get('posterUploadDone') == 'ongoing') {
      switch (component) {
        case 'i':
          return 'fa-spinner fa-pulse';
          break;
        case 'btn':
          return 'btn-warning';
          break;
      }
    } else {
      switch (component) {
        case 'i':
          return 'fa-cloud-upload';
          break;
        case 'btn':
          return 'btn-info';
          break;
      }
    }
  }
});

Template.Poster.events({
  'click #btnPosterClose': function(event) {
    // Close modal and reset all session variables
    event.preventDefault();
    Session.set('dataUrl', '');
    Session.set('posterUploadDone', 'no');
    Modal.hide('Poster');
  },
  'change :file': function(event) {
    // After clicking 'Choisir' button, grab event target and analyze file
    var files = event.target.files;
    if (files.length === 0) {
      return;
    }
    var file = files[0];
    // Size limit of 2mb
    if (file.size > 2097152) {
      Session.set('imageName', 'Fichier trop gros, 2 mb max');
      return;
    }
    // Grab filename and size and send back for textbox
    var tempImageName = file.name + ' (' + Math.round((file.size / 1024 / 1024) * 1000) / 1000 +' mb)';
    Session.set('imageName', file.name);
    Session.set('imageNameSize', tempImageName);
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
      var dataUrl = event.target.result;
      Session.set('dataUrl', dataUrl);
    }
    fileReader.readAsDataURL(file);
  },
  'submit': function(event, template) {
    event.preventDefault();
    Session.set('posterUploadDone', 'ongoing');
    Meteor.call('showsUploadPoster', Session.get('dataUrl'), Session.get('imageName'), function(error, result) {
      if (error) {
        console.log(error);
      } else {
        Session.set('imageUrl', Session.get('dataUrl'));
        Session.set('posterUploadDone', 'yes');
        Session.set('posterId', result);

        toastr['success']('Affiche sauvegardée sur le serveur: ' + Session.get('imageName'));
        Modal.hide('Poster');
      }
    });
  }
});
