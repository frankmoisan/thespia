var DatabaseItemsCollection = new Mongo.Collection("databaseitems");

DatabaseItemsCollection.allow({
    insert: function () {
        return true;
    },
    update: function () {
        return true;
    },
    remove: function () {
        return true;
    }
});

ShowList = new SimpleSchema({
    showId: {
        type: String,
        optional: true
    }
});

DatabaseItemsCollectionSchema = new SimpleSchema({
    entryId: {
        type: String,
        optional: true
    },
    contactId: {
        type: String,
        optional: true
    },
    showList: {
        type: [ShowList],
        optional: true
    },
    itemName: {
        type: String,
        optional: true
    },
    itemType: {
        type: String,
        optional: true
    },
    itemCost: {
        type: Number,
        decimal: true,
        optional: true
    },
    itemSource: {
        type: String,
        optional: true
    }
});

DatabaseItemsCollection.attachSchema(DatabaseItemsCollectionSchema);

export default DatabaseItems = DatabaseItemsCollection;