import './ShowDateTime.html'

Template.ShowDateTime.onRendered(function() {
  const nowDate = new Date();
  const today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
  this.$('.dtDate').datetimepicker({
    locale: 'fr-ca',
    format: 'dddd, D MMMM YYYY',
    minDate: today
  });
  this.$('.dtTime').datetimepicker({
    locale: 'fr-ca',
    format: 'LT'
  });
});
