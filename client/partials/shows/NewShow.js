import './NewShow.html';
const moment = require('moment');
const fr = require('moment/locale/fr');
Meteor.subscribe('AllDatabase');

Template.NewShow.onRendered(function() {
  Session.set('newshow-getdatenumber', 1);
});

Template.NewShow.events({
  'click .newshowPlusDate': function(event) {
    // Add show dates
    event.preventDefault();
    let currentNumberOfDates = Session.get('newshow-getdatenumber');
    currentNumberOfDates++;
    Session.set('newshow-getdatenumber', currentNumberOfDates);
  },
  'click .newshowMinusDate': function(event) {
    // Remove show dates
    event.preventDefault();
    let currentNumberOfDates = Session.get('newshow-getdatenumber');
    currentNumberOfDates--;
    Session.set('newshow-getdatenumber', currentNumberOfDates);
  },
  'click #imgPoster': function(event) {
    // Open modal to upload new show poster
    Session.set('dataUrl', '');
    Session.set('imageUrl', '');
    Modal.show('Poster');
  },
  'reset .newshow': function(event) {
    Session.set('newshow-getdatenumber', 1);
  },
  'submit .newshow': function(event) {
    event.preventDefault();
    const target = event.target;

    // Capture show information
    var newShowData = {
      name: target.newshowName.value,
      desc: target.newshowDesc.value,
      stars: target.newshowStars.value,
      costPresale: target.newshowPresaleCost.value,
      costDoor: target.newshowDoorCost.value,
      costChildren: target.newshowChildrenCost.value,
      internalCost: target.newshowInternalCost.value,
      internalMemo: target.newshowInternalMemo.value,
      poster: Session.get('posterId'),
      author: Meteor.userId()
    };

    // Do the same thing with the show dates by looping through all fields
    var newShowDates = []
    for (var i = 0; i < Session.get('newshow-getdatenumber'); i++) {
      //var newShowDatesTemp = new Date(document.getElementById('newshowDate' + i).value + ' ' + document.getElementById('newshowTime' + i).value)
      var newShowDatesTemp = moment(document.getElementById('newshowDate' + i).value + ' ' + document.getElementById('newshowTime' + i).value, 'dddd, D MMMM YYYY LT');

      newShowDates[i] = {
        date: newShowDatesTemp.toDate()
      };
    }

    // Save the new show information to the database
    Meteor.call('showsCreateShow', newShowData, newShowDates, function(error, result) {
      if (error) {
        console.log(error);
        toastr.error("Une erreur s'est produite lors de la sauvegarde du spectacle.");
      } else {
        // Great success! Show saved to database with all moving parts
        toastr.success("Spectacle sauvegardé avec succès!");

        // Make all session variables undefined so it won't be such a mess in here
        Session.set('newshow-getdatenumber', undefined);
        Session.set('dataUrl', undefined);
        Session.set('imageUrl', undefined);
        Session.set('imageName', undefined);
        Session.set('imageNameSize', undefined);
        Session.set('posterUploadDone', undefined);
        Session.set('posterId', undefined);

        // Go back to All Shows
        Session.set('shows-current-location', 'allshows');
      }
    });
  }
});

Template.NewShow.helpers({
  getDateNumber: function() {
    return Session.get('newshow-getdatenumber');
  },
  getPoster: function() {
    if (Session.get('imageUrl')) {
      return Session.get('imageUrl');
    } else {
      return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAMSURBVBhXYzh8+DAABJYCSsTDBsYAAAAASUVORK5CYII=";
    }
  },
  newshowMinusDateDisabled: function() {
    if (Session.get('newshow-getdatenumber') == 1) {
      return 'disabled';
    } else {
      return;
    }
  },
  loopCount: function(count) {
    var countArr = [];
    for (var i = 0; i < count; i++) {
      countArr.push(i);
    }
    return countArr;
  }
})
