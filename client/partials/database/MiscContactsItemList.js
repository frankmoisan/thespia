import './MiscContactsItemList.html';
var handle = Meteor.subscribe('AllDatabase');
var currentRoute;


Template.MiscContactsItemList.onCreated(function () {
  this.typeSelected = new ReactiveVar('Type');
  this.sourceSelected = new ReactiveVar('Source');
  this.itemList = new ReactiveVar('');
  this.originalDisplayType = new ReactiveVar(true);
  this.originalDisplaySource = new ReactiveVar(true);
});

Template.MiscContactsItemList.onRendered(function () {
  FlowRouter.watchPathChange();
  currentRoute = FlowRouter.current().route.name;
  if (currentRoute == 'misc.single') {
    if (FlowRouter.getParam('_id')) {
      var entryId = FlowRouter.getParam('_id');
      this.autorun(function (thisComp) {
        if (handle.ready()) {
          var currentEntry = Database.findOne({ entryId: entryId });
          Template.instance().itemList.set(currentEntry.itemList);
          thisComp.stop();
        }
      });
    }
  } else if (currentRoute == 'misc.new') {
    Template.instance().typeSelected.set('Type');
    Template.instance().sourceSelected.set('Source');
    Template.instance().itemList.set('');
  }

});

Template.MiscContactsItemList.helpers({
  typeSelected: function () {
    return Template.instance().typeSelected.get();
  },
  sourceSelected: function () {
    return Template.instance().sourceSelected.get();
  },
  item: function (item, counter) {
    if (handle.ready()) {
      switch (item) {
        case 'name':
          if (Template.instance().itemList.get()[counter] && Template.instance().itemList.get()[counter].itemName) {
            return Template.instance().itemList.get()[counter].itemName;
          } else {
            return;
          }
          break;
        case 'type':
          if (Session.get('resetItemList')) {
            Template.instance().typeSelected.set('Type');
            Template.instance().sourceSelected.set('Source');
            Template.instance().itemList.set('');
            return 'Type';
          }
          if (Template.instance().originalDisplayType.get() && Template.instance().itemList.get()[counter] && Template.instance().itemList.get()[counter].itemType) {
            return Template.instance().itemList.get()[counter].itemType;
          } else {
            return Template.instance().typeSelected.get();
          }
          break;
        case 'cost':
          if (Template.instance().itemList.get()[counter] && Template.instance().itemList.get()[counter].itemCost) {
            return Template.instance().itemList.get()[counter].itemCost;
          } else {
            return;
          }
          break;
        case 'source':
          if (Session.get('resetItemList')) {
            Session.set('resetItemList', false);
            return 'Source';
          }
          if (Template.instance().originalDisplaySource.get() && Template.instance().itemList.get()[counter] && Template.instance().itemList.get()[counter].itemSource) {
            return Template.instance().itemList.get()[counter].itemSource;
          } else {
            return Template.instance().sourceSelected.get();
          }
          break;
        default:
          return;
      }
    } else {
      return;
    }
  }
});

Template.MiscContactsItemList.events({
  'click .sourceSelector': function (event) {
    var target = event.target.id;
    Template.instance().originalDisplaySource.set(false);
    switch (target) {
      case 'sourceDonate':
        Template.instance().sourceSelected.set('Donation');
        break;
      case 'sourceLend':
        Template.instance().sourceSelected.set('Lend');
        break;
      case 'sourceRent':
        Template.instance().sourceSelected.set('Rental');
        break;
      case 'sourcePurchase':
        Template.instance().sourceSelected.set('Purchase');
        break;
      default:
        Template.instance().sourceSelected.set('Source');
        break;
    }
  },
  'click .typeSelector': function (event) {
    var target = event.target.id;
    Template.instance().originalDisplayType.set(false);
    switch (target) {
      case 'typeCostumes':
        Template.instance().typeSelected.set('Costumes');
        break;
      case 'typeProps':
        Template.instance().typeSelected.set('Props');
        break;
      case 'typeBeauty':
        Template.instance().typeSelected.set('Beauty Products');
        break;
      case 'typeSetPieces':
        Template.instance().typeSelected.set('Set Pieces');
        break;
      case 'typeTech':
        Template.instance().typeSelected.set('Tech Equipment');
        break;
      case 'typeCashDonation':
        Template.instance().typeSelected.set('Cash Donation');
        break;
      case 'typeAuctions':
        Template.instance().typeSelected.set('Auction Item');
        break;
      default:
        Template.instance().typeSelected.set('Type');
        break;
    }
  },
  'click .btnContactDelete': function (event) {
    console.log('Form was reset!');
  }
});
