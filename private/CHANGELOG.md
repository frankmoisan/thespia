# THESPIA CHANGELOG #

### [1.0.7] - 2017-02-25
#### Added
- Link to homepage on navbar brand
- RLT stamp image on Search Page

#### Changed
1. General
    - Updated code to reflect app name change (from CMA-RLT DB to Thespia)
    - Fixed bug on User Management > Permissions page where certain checkboxes wouldn't be checked correctly
    - Updated English localization
    - Increased width of every page to 90% (up from 70%)
    - Updated toasters to show pertinent error messages
2. Search Page
    - ESC now clears the results table instead of searching everything
    - Improved 'Delete' button feedback and results after deleting
    - Made search results somewhat more responsive for smaller displays
    - Replaced tags with real Bootstrap labels
3. Volunteers
    - Volunteer form now resets properly upon clicking 'Save and Add New'
    - Volunteer form now changes URL properly upon clicking 'Save'
    - Modified headshots to force a grey picture if no image is selected
    - Made headshot image responsive
    - Updated code to reflect Volunteer tag change (from 'Design' to 'Prod. Staff')
    - Added feedback to 'Delete' button
4. Misc Contacts
    - Misc Contact form now resets properly upon clicking 'Save and Add New'
    - Misc Contact form now changes URL properly upon clicking 'Save'
    - Added feedback to 'Delete' button
- - -

### [1.0.6] - 2017-02-20
#### Added
- 'Hair' field to Volunteer form
- 'Clear Picture' option to Volunteer form

#### Changed
- Reworked how headshots are handled
- - -

### [1.0.5] - 2017-02-20
#### Added
- Version number on Login and Admin page
- Link to CHANGELOG from version number on Admin page only
- Secure connections and SSL Certificate
- Tags for 'Potential Items' and 'Positions' in the search result table

#### Changed
- Sizing of Search results table to accomodate new Tags column
- - -

### [1.0.4] - 2017-02-20
#### Added
1. Volunteers Form
    * Basic auto-formatting for phone number fields
- - -

### [1.0.3] - 2017-02-19
#### Changed
1. Volunteers Form
    * Removed Number requirement from 'Age' field, changed to String
- - -

### [1.0.2] - 2017-02-19
#### Changed
1. Volunteers Form
    * Changed 'Weight' field for 'Build', removed 'lbs' trailing addon
    * Removed Number requirement from 'Build' field, changed to String
    * Reworked autocomplete to try and bypass browser (was adding information for 'State' in the 'Height In' field)
- - -

### [1.0.1] - 2017-02-19
#### Changed
- Fixed bug in user creation page that would prevent any user from being created.
- - -

### [1.0.0] - 2017-02-19
#### Added
- Initial release