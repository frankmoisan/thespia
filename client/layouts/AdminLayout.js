import './AdminLayout.html';

Meteor.subscribe('AllUsers', function() {
    Session.set('dbLoaded', true);
});


Template.AdminLayout.helpers({
  allusers: function() {
    return Meteor.users.find().sort({datefield: -1});
  },
  dbLoaded: function() {
    return Session.get('dbLoaded');
  },
  release: function() {
    return Meteor.settings.public.releaseName + ' ' + Meteor.settings.public.releaseVersion;
  }
});
