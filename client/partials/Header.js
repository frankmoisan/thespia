import './Header.html';

handle = Meteor.subscribe('CurrentUser');

Template.Header.onCreated(function(){
  Session.set('navDashboard', '');
  Session.set('navBoxOffice', '');
  Session.set('navShows', '');
  Session.set('navDatabase', '');
  Session.set('navAdmin', '');
});

Template.Header.helpers({
  navDashboard: function() {
    return Session.get('navDashboard');
  },
  navBoxOffice: function() {
    return Session.get('navBoxOffice');
  },
  navShows: function() {
    return Session.get('navShows');
  },
  navDatabase: function() {
    return Session.get('navDatabase');
  },
  navAdmin: function() {
    return Session.get('navAdmin');
  },
  getPerms: function(section) {
    const thisUserPerms = Meteor.users.findOne({_id: Meteor.userId()});
    const perms = thisUserPerms && thisUserPerms.profile && thisUserPerms.profile.permissions;  // Trying to guard, failed horribly
    switch(section) {
      case 'Reservations':
        if (handle.ready()) {
          return perms.boxoffice.access;
        } else {
          return;
        }
        break;
      case 'Shows':
        if (handle.ready()) {
          return perms.shows.access;
        } else {
          return;
        }
        
        break;
      case 'Clients':
        if (handle.ready()) {
          return perms.database.access;
        } else {
          return;
        }
        
        break;
      case 'Customers':
        if (handle.ready() && perms.database.access) {
          return perms.database.customers.access;
        } else {
          return;
        }
        break;
      case 'Volunteers':
        if (handle.ready() && perms.database.access) {
          return perms.database.volunteers.access;
        } else {
          return;
        }
        break;
      case 'Misc':
        if (handle.ready() && perms.database.access) {
          return perms.database.misc.access;
        } else {
          return;
        }
        break;
      case 'Search':
        if (handle.ready() && perms.database.access) {
          return perms.database.search;
        } else {
          return;
        }
        break;
      case 'Admin':
        if (handle.ready()) {
          return perms.admin;
        } else {
          return;
        }
        break;
      default:
        return false;
    }
  }
});

Template.Header.events({
  'click .navAccueil': ()=> {
    SessionClear();
    Session.set('navDashboard', 'active');
  },
  'click .navReservations': ()=> {
    SessionClear();
    Session.set('navBoxOffice', 'active');
  },
  'click .navSpectacles': ()=> {
    SessionClear();
    Session.set('navShows', 'active');
  },
  'click .navClients': ()=> {
    SessionClear();
    Session.set('navDatabase', 'active');
  },
  'click .navAdmin': ()=> {
    SessionClear();
    Session.set('navAdmin', 'active');
  }
});

function SessionClear() {
  Session.set('navDashboard', '');
  Session.set('navBoxOffice', '');
  Session.set('navShows', '');
  Session.set('navDatabase', '');
  Session.set('navAdmin', '');
}
