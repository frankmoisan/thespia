import './ResetPassword.html';

Template.ResetPassword.helpers({
  user: function () {
    var userSession = Session.get('usercontrol');
    return Meteor.users.findOne({ _id: userSession });
  }
});

Template.ResetPassword.events({
  'submit .resetpassword': function (event) {
    event.preventDefault();
    const newPassword = event.target.resetPassword.value;

    if (newPassword == '') {
      toastr.error('The password field cannot be empty!', 'Error');
    } else {
      const passwordResetUser = Meteor.users.findOne({ _id: Session.get('usercontrol') }, { fields: { username: 1 } });
      Meteor.call('adminResetPassword', Session.get('usercontrol'), newPassword, function (error, success) {
        if (error) {
          toastr.error('Great sadness...', 'Error');
          console.log(error);
        } else {
          toastr['success']("Password changed for user '" + passwordResetUser.username + "'.", "Success");
        }
      });
      Modal.hide('ResetPassword');
      Session.set('usercontrol', '');
    }
  }
});
