var ShowDatesCollections = new Mongo.Collection('showdates');

ShowDatesCollections.allow({
  insert: function(){
    return true;
  },
  update: function(){
    return true;
  },
  remove: function(){
    return true;
  }
});

ShowDatesCollectionsSchema = new SimpleSchema({
  showId: {
    type: String
  },
  showDate: {
    type: Date
  }
/*  showDate: {
    type: String
  },
  showTime: {
    type: String
  }*/
});

ShowDatesCollections.attachSchema(ShowDatesCollectionsSchema);

export default ShowDates = ShowDatesCollections;
