import './AllShows.html';

//Meteor.subscribe('AllDatabase');

Template.AllShows.onCreated(function () {
    Session.set('dbSearching', false)
    Session.set('dbFoundResults', false);
    $(document).on('keyup', function (keyEvent) {
        if (keyEvent.keyCode == 27) {
            $('#showSearch').val('');
            Session.set('dbFoundResults', false);
            Session.set('dbSearching', false);
            FlowRouter.go('shows.search');
        }
    });
});

Template.AllShows.onDestroyed(function () {
    $(document).off('keyup');
});

Template.AllShows.helpers({
    foundResults: function () {
        return Session.get('dbFoundResults');
    },
    searching: function () {
        return Session.get('dbSearching');
    },
    results: function (tag) {
        if (tag == 'tags') {
            console.log('Tags!')
        } else {
            return Session.get('searchResults');
        }
    },
    isDisabled: function() {
        var thisUser = Meteor.user().profile.permissions.shows;
        if (!thisUser.volunteers.delete) {
            return 'disabled';
        }

    }
});

Template.AllShows.events({
    'submit .searchform': function (event) {
        event.preventDefault();
        search(event.target.showSearch.value);
    },
    'click #btnReturnShows': function (event) {
        FlowRouter.go('shows.search');
    },
    'click #btnNewShow': function (event) {
        FlowRouter.go('shows.new');
    },
    'click .btn-delete': function (event) {
        event.preventDefault();
        /*var thisBtn = $('#' + event.currentTarget.id);
        var thisSpan = $('#span' + event.currentTarget.id);
        if (thisBtn.hasClass('btnPost')) {
            Meteor.call('dbDeleteContact', event.currentTarget.id, function (error, success) {
                if (error) {
                    toastr.error('Could not delete contact!', 'Error');
                    console.log(error);
                } else {
                    toastr.success('Contact deleted.', 'Success');
                    search($('#dbSearch').val());
                }
            });
        }
        if (thisBtn.hasClass('btnPre')) {
            thisBtn.removeClass('btn-default btnPre').addClass('btn-danger btnPost');
            thisSpan.text('CONFIRM?');
            setTimeout(function () {
                thisBtn.removeClass('btn-danger btnPost');
                thisBtn.addClass('btn-default btnPre');
                thisSpan.text('Delete');
            }, 5000);
        }*/
    },
    'click .tag': function (event) {
        event.preventDefault();
    }
});

function search(event) {
    Session.set('dbFoundResults', false);
    Session.set('dbSearching', true);

    Meteor.call('searchShows', event, function (error, result) {
        if (error) {
            toastr.error('Could not get results');
            console.log(error);
        } else {
            Session.set('searchResults', result);
        }
    });

    Session.set('dbFoundResults', true);
    Session.set('dbSearching', false);
}