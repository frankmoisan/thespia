import './DeleteContact.html';

Template.DeleteContact.helpers({
    contact: function () {
        var contactInfo = Session.get('deleteContact');
        return Database.findOne({ entryId: contactInfo });
    }
});

Template.DeleteContact.events({
    'submit .deletecontact': function (event) {
        event.preventDefault();
        Meteor.call('dbDeleteContact', Session.get('deleteContact'), function (error, success) {
            if (error) {
                toastr.error('Could not delete contact!', 'Error');
                console.log(error);
            } else {
                console.log(success);
                toastr.success('Contact deleted.', 'Success');
                FlowRouter.go(FlowRouter.current());
            }
        });
        Session.set('deleteContact', '');
        Modal.hide('DeleteContact');
    }
});
