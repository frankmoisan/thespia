import './DeleteUser.html';

Template.DeleteUser.helpers({
  user: function() {
    var userSession = Session.get('usercontrol');
    return Meteor.users.findOne({_id: userSession});
  }
});

Template.DeleteUser.events({
  'submit .deleteuser': function(event) {
    event.preventDefault();
    let userToDelete = Meteor.users.findOne({_id: Session.get('userSession')}, {fields: {username: 1}});
    Meteor.call('adminDeleteUser', Session.get('usercontrol'), function (error, result) {
      if (error) {
        toastr.error(error.result, 'Error');
        console.log(error);
      } else {
        Modal.hide('DeleteUser');
        toastr['success']('User "' + userToDelete.username + '" deleted.', "Success");
        Session.set('usercontrol', '');
      }
    });
  }
});
